def single2(cart_code):
  try:
    from dashboard.utils.date import (timedelta_to_hours, hours_to_timedelta)
    from datetime import datetime, timedelta
    from apps.models_helper.cart import (Cart, CartPhoto, CartTitipkirimin, CartDetail, CartLog, CartMovement, CartCost, CustomClearance)

    x = CartLog.objects.filter(cart__cart_code = cart_code)
    betul = []
    salah = []
    log = []
    hari = []
    correct_time = []
    wrong_time = []
    cart_log_id_update = []
    for k in x: log.append( k.log_date)
    for k in x: hari.append(timedelta(days= hours_to_timedelta(k.lead_time)['days'] , hours= hours_to_timedelta(k.lead_time)['hours'], minutes= hours_to_timedelta(k.lead_time)['minutes']))

    for k in range(len(log)):
        try: 
                betul.append(log[k+1] + hari[k])
                salah.append(log[k])
        except:pass

    for b in range(len(betul)):
      try:
          if betul[b].strftime("%d") != salah[b].strftime("%d"):
                wrong_time.append(salah[b])
                correct_time.append(betul[b])
                log_error = CartLog.objects.filter(log_date = salah[b]).values_list('cart_log_id', flat=True)
                cart_log_id_update.append(log_error[0])
      except: pass

    print(wrong_time[0])
    print(correct_time[0])
    print(cart_log_id_update[0])
    CartLog.objects.filter(cart_log_id = cart_log_id_update[0]).update(log_date = correct_time[0])
  except:pass
  
def update ():
  wrong_data = ['CVCX1332','C2263688','CTML3725','CRWW3731','CEVX3473','CTAL4724','C7KJ8168','CFSX9589','C1EW9742','C1LW10045','CCNX10520','CRPW10936','CSXX10991','C98J11270','CEXB11407','CEXB20207','C11L21966','CPWL20206','CYTJ33445','CEEW37022','CPPW139766','CMML36955',]
  for k in wrong_data:
    single2(k)
